/// <reference types="vitest" />

import react from "@vitejs/plugin-react";
import tsconfigPaths from "vite-tsconfig-paths";
import { defineConfig } from "vitest/config";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), tsconfigPaths()],
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "./setupTests.ts",
    css: true,
    reporters: ["junit", "verbose"],
    outputFile: {
      junit: "./vitest-report.xml",
    },
    coverage: {
      reporter: ["text", "json", "html"],
      include: ["src/**/*"],
      exclude: [],
    },
  },
});

// confirmar se o arquivo vite.d.ts tem as seguintes importações de tipos

/// <reference types="vite/client" />
/// <reference types="@testing-library/jest-dom" />
