const path = require("path");

const buildEslintCommand = (filenames) =>
  `next lint --fix --file ${filenames
    .map((f) => path.relative(process.cwd(), f))
    .join(" --file ")}`;

const buildPrettierCommand = (filenames) =>
  `pnpm prettier --write ${filenames.join(" ")}`;

const buildSassCommand = (filenames) => `pnpm stylelint ${filenames.join(" ")}`;

module.exports = {
  "*.{js,jsx,ts,tsx}": [buildEslintCommand],
  "**/*.(ts|tsx|js|md|json)": [buildPrettierCommand],
  "*.scss": [buildSassCommand],
};
