import { describe, expect, it } from "vitest";

import isEqualPrimitive from "./isEqualPrimitive";

describe("isEqualPrimitive function", () => {
  it("should return true when comparing equal strings", () => {
    expect(isEqualPrimitive("ngockhoi96", "ngockhoi96")).toBe(true);
  });

  it("should return false when comparing different strings", () => {
    expect(isEqualPrimitive("ngockhoi96", "ngocKhoi96")).toBe(false);
    expect(isEqualPrimitive("ngockhoi96", "ngockhoi")).toBe(false);
  });

  it("should return true when comparing equal numbers", () => {
    expect(isEqualPrimitive(69, 69)).toBe(true);
    expect(isEqualPrimitive(-4, -4)).toBe(true);
  });

  it("should return false when comparing different numbers", () => {
    expect(isEqualPrimitive(69, 96)).toBe(false);
    expect(isEqualPrimitive(-26, 26)).toBe(false);
  });

  it("should return true when comparing equal booleans", () => {
    expect(isEqualPrimitive(true, true)).toBe(true);
  });

  it("should return false when comparing different booleans", () => {
    expect(isEqualPrimitive(true, false)).toBe(false);
  });

  it("should return true when comparing undefined values", () => {
    expect(isEqualPrimitive(undefined, undefined)).toBe(true);
  });

  it("should return true when comparing null values", () => {
    expect(isEqualPrimitive(null, null)).toBe(true);
  });

  it("should return false when comparing NaN values", () => {
    expect(isEqualPrimitive(NaN, NaN)).toBe(false);
  });
});
