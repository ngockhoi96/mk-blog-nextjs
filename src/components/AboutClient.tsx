"use client";

import { useEffect, useState } from "react";

function AboutClient() {
  const [posts, setPosts] = useState<
    {
      userId: number;
      id: number;
      title: string;
      body: string;
    }[]
  >([]);

  const fetchPosts = async () => {
    const postApi = await fetch("https://jsonplaceholder.typicode.com/posts");

    const data = await postApi.json();

    setPosts(data);

    console.log({ data });
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div>
      {posts.map((post) => (
        <div key={post.id}>
          {post.id}
          <p>{post.title}</p>
          <div>{post.body}</div>
        </div>
      ))}
    </div>
  );
}

export default AboutClient;
