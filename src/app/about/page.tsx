import Link from "next/link";

import AboutClient from "@/components/AboutClient";

async function AboutPage() {
  const data = await fetch("https://jsonplaceholder.typicode.com/todos/1");

  const result = await data.json();

  console.log({ data });

  return (
    <div>
      <div>
        <h1>About</h1>
        <Link href="/">Home</Link>

        <pre>{result.title}</pre>
      </div>

      <div>
        <AboutClient />
      </div>
    </div>
  );
}

export default AboutPage;
