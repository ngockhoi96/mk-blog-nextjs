NETWORK_NAME=my_network
COMPOSE_FILE_DEVELOPMENT=docker-compose.dev.yml

.PHONY: network
network:
	docker network create $(NETWORK_NAME)

.PHONY: build
build:
	docker-compose -f $(COMPOSE_FILE_DEVELOPMENT) build

.PHONY: up
up:
	docker-compose -f $(COMPOSE_FILE_DEVELOPMENT) up

.PHONY: down
down:
	docker-compose -f $(COMPOSE_FILE_DEVELOPMENT) down

.PHONY: build-up
build-up: build up

.PHONY: all
all: network build up
